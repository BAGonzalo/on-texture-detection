# Saliency detection [WIP]

Research on saliency detection methods & PyTorch implementation of [Surface Detect Saliency of Magnetic Tile](https://www.researchgate.net/publication/325882869_Surface_Defect_Saliency_of_Magnetic_Tile) propose architecture.

Author's C++ saliency-toolkit: https://github.com/abin24/Saliency-detection-toolbox

## Summary

## Setting up

To run the code and notebooks, use the image dockerfile provided [CPU-only support], as follows:

[1] Install [Docker Engine](https://www.docker.com/community-edition#/download); and clone the repository.

[2] Build the image, and run the environment by:

```
cd 
docker build -t cpu -f dockerfile_cpu .
docker run -it -v {global-path-to-repo}/test:/test -p 9000:9000 --user root --shm-size 16G --name test cpu
cd test
jupyter notebook --ip 0.0.0.0 --no-browser --port 9000 --allow-root 

```

To enter the running container from another console window, run:

```
docker exec -it test bin/bash
```

[3] Access the Jupyter notebook tree at http://localhost:9000/, and enter the first token provided by the console -e.g.:

```
[I 13:30:53.425 NotebookApp] The Jupyter Notebook is running at:
[I 13:30:53.426 NotebookApp] http://809b15d36bae:9000/?token=5b849c9e10fc2671579c113e270f3953ad33113120ce28d6
```

The repo will be visualised as a files tree in your browser.

[4] Open and run the notebook/s.

## Authors

* **BAGonzalo** - https://gitlab.com/BAGonzalo (bagonzalo@gmail.com)