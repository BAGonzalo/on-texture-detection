import torch
from torch import nn
from switchnorm import SwitchNorm

def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        m.weight.data.normal_(0.0, 0.02)
    elif classname.find('BatchNorm2d') != -1:
        m.weight.data.normal_(1.0, 0.02)
        m.bias.data.fill_(0)

# Define a resnet block
class ResnetBlock(nn.Module):
    def __init__(self, dim, padding_type, norm_layer=nn.BatchNorm2d, activation=nn.ReLU(True), use_dropout=False):
        super(ResnetBlock, self).__init__()
        self.conv_block = self.build_conv_block(dim, padding_type, norm_layer, activation, use_dropout)

    def build_conv_block(self, dim, padding_type, norm_layer, activation, use_dropout):
        conv_block = []
        p = 0
        if padding_type == 'reflect': conv_block += [nn.ReflectionPad2d(1)]
        elif padding_type == 'replicate': conv_block += [nn.ReplicationPad2d(1)]
        elif padding_type == 'zero': p = 1
        else: raise NotImplementedError('padding [%s] is not implemented' % padding_type)

        conv_block += [nn.Conv2d(dim, dim, kernel_size=3, padding=p), 
                       norm_layer(dim), 
                       activation]
        
        if use_dropout: conv_block += [nn.Dropout(0.5)]

        p = 0
        if padding_type == 'reflect': conv_block += [nn.ReflectionPad2d(1)]
        elif padding_type == 'replicate': conv_block += [nn.ReplicationPad2d(1)]
        elif padding_type == 'zero': p = 1
        else: raise NotImplementedError('padding [%s] is not implemented' % padding_type)
        
        conv_block += [nn.Conv2d(dim, dim, kernel_size=3, padding=p), norm_layer(dim)]

        return nn.Sequential(*conv_block)

    def forward(self, x):
        out = x + self.conv_block(x)
        return out
    
class AutoEncoder(nn.Module):
    def __init__(self, conf_file, input_nc, output_nc, m_type, 
                 ngf=64, n_downsampling=3, n_blocks=None, padding_type='reflect', n_classes=2):
        
#         assert(n_blocks >= 0)
        super(AutoEncoder, self).__init__()        
        
        activation = nn.ReLU(True) 
        # nn.LeakyReLU(True)        

        model = [get_norm_layer(input_nc),
                 nn.ReplicationPad2d(3), 
                 nn.Conv2d(input_nc, ngf, kernel_size=7, padding=0), 
                 get_norm_layer(ngf), 
                 activation]
        
        ### downsample
        for i in range(n_downsampling):
            mult = 2**i
            model += [nn.Conv2d(ngf * mult, ngf * mult * 2, kernel_size=3, stride=2, padding=1),
                      get_norm_layer(ngf * mult * 2), 
                      activation]

        ### resnet blocks
        if n_blocks:
            mult = 2**n_downsampling
            for i in range(n_blocks):
                model += [ResnetBlock(ngf * mult, padding_type=padding_type, activation=activation)]
        
        if m_type == 'E':
            model += [
                      nn.Conv2d(ngf * mult * 2, 1, kernel_size=1),
                      # get_norm_layer(1),
                      # activation,
#                       nn.ConvTranspose2d(1, 1, kernel_size=3, stride=2, padding=1, output_padding=1),
                      # nn.Linear(ngf * mult, n_classes)
#                       nn.Linear(n_classes, 10),
                      nn.Linear(ngf * mult * 2, 1),
                      nn.Sigmoid()
                      ]
        
        elif m_type == 'ED':
            ### upsample         
            for i in range(n_downsampling):
                mult = 2**(n_downsampling - i)
                model += [nn.ConvTranspose2d(ngf * mult, int(ngf * mult / 2), kernel_size=3, stride=2, padding=1, output_padding=1),
                          get_norm_layer(int(ngf * mult / 2)), 
                          activation]

            model += [nn.ReplicationPad2d(3), 
                      nn.Conv2d(ngf, output_nc, kernel_size=7, padding=0), 
                      nn.Tanh()]        
        
        self.model = nn.Sequential(*model)
            
    def forward(self, input):
        return self.model(input) 

def get_norm_layer(input_nc, norm_type='switch'):
    if norm_type == 'switch': norm_layer = SwitchNorm(input_nc) # momentum=0.997 - check momentum & affine 
    elif norm_type == 'batch': norm_layer = nn.BatchNorm2d(input_nc, affine=True) 
    elif norm_type == 'instance': norm_layer = nn.InstanceNorm2d(input_nc, affine=False)
    else: raise NotImplementedError('normalization layer [%s] is not found' % norm_type)
    return norm_layer

def define_net(conf_file, input_nc, output_nc, ngf, model='autoencoder', n_downsample_global=3, n_blocks_global=None, 
               n_local_enhancers=1, n_blocks_local=3, norm='instance', padding_type='reflect', n_classes=2, gpu_ids=[]):    
    
#     norm_layer = get_norm_layer(input_nc, norm_type=norm)     
    if model == 'autoencoder': 
        net = AutoEncoder(conf_file, input_nc, output_nc, 'D', ngf, n_downsample_global, n_blocks_global, padding_type, n_classes) 
    elif model == 'encoder': 
        net = AutoEncoder(conf_file, input_nc, output_nc, 'E', ngf, n_downsample_global, n_blocks_global, padding_type, n_classes)  
    else: raise('arg. model should be autoencoder | encoder')
    
    net.apply(weights_init)
#     print(net)
    return net

# class ResnetBlock(nn.Module):
#     def __init__(self, dim, padding_type, norm_layer=nn.BatchNorm2d, activation=nn.ReLU(True), use_dropout=False):
#
#         super(ResnetBlock, self).__init__()
#         self.conv_block = self.build_block(dim, padding_type, norm_layer, activation, use_dropout)
#
#     def build_block(self, dim, padding_type, norm_layer, activation, use_dropout):
#         conv_block = []
#
#         return nn.Sequential(*conv_block)
#
#     def forward(self, x):
#         out = x + self.conv_block(x)
#         return out