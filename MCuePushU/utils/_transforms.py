import numpy as np    
import cv2
import numbers
import random
import collections

class RandomCrop(object):
    def __init__(self, size, padding=True, pad_if_needed=False, fill=0, padding_mode='constant'):
        if isinstance(size, numbers.Number):
            self.size = (int(size), int(size))
        else:
            self.size = size
        self.padding = padding
        self.pad_if_needed = pad_if_needed
        self.fill = fill
        self.padding_mode = padding_mode

    @staticmethod
    def get_params(img, crop_size, state):
        """Get parameters for ``crop`` for a random crop.
        Args:
            img (PIL Image): Image to be cropped.
            output_size (tuple): Expected output size of the crop.
        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for random crop.
        """
        h, w = img.shape[:2]
        th, tw = crop_size
        if w == tw and h == th: return 0, 0, h, w

        random.setstate(state)
        i, j = random.randint(0, h), random.randint(0, w)

        return i, j, th, tw
    
    def crop(self, img, state):
        """ """
        i, j, h, w = self.get_params(img, self.size, state) 
        if self.padding: img = cv2.copyMakeBorder(img, h, h, w, w, cv2.BORDER_REFLECT101) 
        return img[i:i+h, j:j+w, ...]

    def __call__(self, ims):
        """ call function """
        state = random.getstate()
        return [self.crop(i, state) for i in ims]
        
    def __repr__(self):
        return self.__class__.__name__ + '(size={0}, padding={1})'.format(self.size, self.padding)
    
        
class Normalize(object):
    """Normalize a tensor image with mean and standard deviation.
    Given mean: ``(M1,...,Mn)`` and std: ``(S1,..,Sn)`` for ``n`` channels, this transform
    will normalize each channel of the input ``torch.*Tensor`` i.e.
    ``input[channel] = (input[channel] - mean[channel]) / std[channel]``
    .. note::
        This transform acts in-place, i.e., it mutates the input tensor.
    Args:
        mean (sequence): Sequence of means for each channel.
        std (sequence): Sequence of standard deviations for each channel.
    """

    def __init__(self, mean, std):
        self.mean = mean
        self.std = std

    def __call__(self, tensor):
        """
        Args:
            tensor (Tensor): Tensor image of size (C, H, W) to be normalized.
        Returns:
            Tensor: Normalized Tensor image.
        """
        return F.normalize(tensor, self.mean, self.std)

    def __repr__(self):
        return self.__class__.__name__ + '(mean={0}, std={1})'.format(self.mean, self.std)
        
def read_image(path):
    im = cv2.imread(path, cv2.IMREAD_UNCHANGED)
#     if len(im.shape) == 2: im = np.expand_dims(im, 0)
    return im

class Resize(object):
    def __init__(self, new_dims):
        if isinstance(new_dims, int): self.dims = (new_dims, new_dims)
        else: self.dims = new_dims
    def resize_image(self, im):
        return cv2.resize(im, (self.dims))
    def __call__(self, ims):
        return [self.resize_image(i) for i in ims]
        
class FlipY(object):
    def __init__(self):
        pass
    def __call__(self, im):
        return  im[:, ::-1]
        
class FlipX(object):
    def __init__(self):
        pass
    def __call__(self, im):
        return  im[::-1]
        
class FlipC(object):
    def __init__(self):
        pass
    def __call__(self, im):
        return  im[::-1]
        
class RandomRotation(object):
    def __init__(self):
        self.n = np.random.randint(1, 3)
        pass
    def __call__(self, im):
        return np.rot90(im, self.n)
        